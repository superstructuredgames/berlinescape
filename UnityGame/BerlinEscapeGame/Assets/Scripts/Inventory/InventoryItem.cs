﻿using System.Collections.Generic;
using Assets.Scripts.PlayerInteraction;
using UnityEngine;

namespace Assets.Scripts.Inventory {

    
    public class InventoryItem : MonoBehaviour{
        public string displayName;
        public string uiSpriteName;
        public Texture2D cursorTexture;
        public GameObject toTransfer;
        private static readonly Collider2D[] RESULTS = new Collider2D[100];
        private int myIndex;

        public void HandleSelected() {
            Vector2 mid = new Vector2(cursorTexture.width/2.0f, cursorTexture.height/2.0f);
            Cursor.SetCursor(cursorTexture, mid, CursorMode.Auto);
        }

        public void SetIndex(int index) {
            myIndex = index;
        }

        public void HandleUsed(Vector2 position) {
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
            int count = Physics2D.OverlapPointNonAlloc(position, RESULTS);
            for (int i = 0; i < count; i++) {
                InventoryInteractable[] interactables = RESULTS[i].GetComponents<InventoryInteractable>();
                foreach (InventoryInteractable interactable in interactables) {
                    interactable.HandleInteracted(myIndex);
                }
            }
        }
        
    }
}
