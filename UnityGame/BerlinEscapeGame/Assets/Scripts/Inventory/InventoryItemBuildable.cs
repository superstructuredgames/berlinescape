﻿using System.Collections.Generic;
using Assets.Scripts.PlayerInteraction;
using MarkedUi;
using MarkedUi.Buildables;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts.Inventory
{
    public class InventoryItemBuildable : MonoBehaviour, MarkedUiBuildable, IPointerClickHandler, IBeginDragHandler, IEndDragHandler, IDragHandler{
        public string onSelectStartAttrName = "on-item-select-start";
        public string onSelectStopAttrName = "on-item-select-stop";
        public string inventoryIndexAttrName = "item-index";
        [HideInInspector, SerializeField] private string eventStartName;
        [HideInInspector, SerializeField] private string eventEndName;
        [HideInInspector, SerializeField] private string itemIndex;
        [HideInInspector, SerializeField] private GameObject controller;


        public void Build(MarkedUiViewNode node) {
            eventStartName = node.GetStringAttribute(onSelectStartAttrName, null);
            eventEndName = node.GetStringAttribute(onSelectStopAttrName, null);
            itemIndex = node.GetStringAttribute(inventoryIndexAttrName, null);
            controller = node.Controller;
        }
        
        public void OnPointerClick(PointerEventData anEvent) {
            if (eventStartName != null && itemIndex != null && eventEndName != null) {
                controller.SendMessage(eventStartName, itemIndex, SendMessageOptions.DontRequireReceiver);
                StartCoroutine(AddDelayedRelease());
            }
            
        }
        
        public void OnBeginDrag(PointerEventData eventData) {
            if (eventStartName != null && itemIndex != null && eventEndName != null){
                controller.SendMessage(eventStartName, itemIndex, SendMessageOptions.DontRequireReceiver);
            }
        }

        public void OnEndDrag(PointerEventData eventData) {
            HandleReleased(Vector2.zero);
        }

        private IEnumerator<YieldInstruction> AddDelayedRelease() {
            yield return new WaitForEndOfFrame();
            PlayerInteractionController.AddOneTimeOverride(HandleReleased);
        }

        private void HandleReleased(Vector2 position) {
            controller.SendMessage(eventEndName, itemIndex, SendMessageOptions.DontRequireReceiver);
        }

        public void OnDrag(PointerEventData eventData) {
            
        }
    }
}
