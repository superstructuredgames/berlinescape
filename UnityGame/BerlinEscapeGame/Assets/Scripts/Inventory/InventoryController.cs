﻿using System.Collections.Generic;
using System.Security.Principal;
using UnityEngine;

namespace Assets.Scripts.Inventory {
    public class InventoryController : MonoBehaviour {

        private static InventoryController instance;

        public List<InventoryItem> items = new List<InventoryItem>();

        public void Awake() {
            instance = this;
            UpdateIndexes();
        }

        private void UpdateIndexes() {
            for (int i = 0; i < items.Count; i++) {
                items[i].SetIndex(i);
            }
        }

        public static void AddInventoryItem(InventoryItem item) {
            if (item.toTransfer != null) {
                item.toTransfer.transform.parent = instance.transform;
                item.toTransfer.SetActive(false);
            }
            instance.items.Add(item);
            item.SetIndex(instance.items.Count-1);
        }

        public static int GetItemCount() {
            return instance.items.Count;
        }

        public static InventoryItem GetItem(int index) {
            return instance.items[index];
        }

        public static void RemoveItem(int index) {
            instance.items.RemoveAt(index);
            instance.UpdateIndexes();
        }

    }
}
