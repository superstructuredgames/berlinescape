﻿using System;
using UnityEngine;

namespace Assets.Scripts.Inventory {
    public class DemoInventoryInteractable : MonoBehaviour, InventoryInteractable {

        public GameObject root;

        public void HandleInteracted(int itemIndex) {
            Debug.Log("you killed me with the " + InventoryController.GetItem(itemIndex).displayName);
            Destroy(root);
        }
    }
}
