﻿namespace Assets.Scripts.Inventory{
    public interface InventoryInteractable {

        void HandleInteracted(int itemIndex);
    }
}
