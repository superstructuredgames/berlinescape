﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Pathing;
using Assets.Scripts.Perspective;
using Assets.Scripts.PlayerInteraction;
using UnityEngine;

namespace Assets.Scripts.Characters {
    public class PlayerCharacter : MonoBehaviour, Character {

        public PointPathFollower follower;
        public bool isModernPerspective;
        public GameObject viewPrefab;
        public GameObject viewRoot;
        public bool isFaceLeft;

        private GameObject view;
        private Animator viewAnimator;
        private bool wasFaceLeft;
        

        public void Start() {
            CharacterController.AddPlayerCharacter(this);
            view = Instantiate(viewPrefab);
            view.transform.parent = viewRoot.transform;
            view.transform.localPosition = Vector3.zero;
            view.transform.localRotation = Quaternion.identity;
            view.transform.localScale = Vector3.one;
            viewAnimator = view.GetComponent<Animator>();
        }

        public void PathToPoint(Vector2 point, Action onArrive) {
            PathableArea[] areas = FindObjectsOfType<PathableArea>();
            PathableArea pathArea = null;
            foreach (PathableArea area in areas) {
                if (area.ContainsPoint(follower.transform.position)) {
                    pathArea = area;
                }
            }
            if (pathArea != null) {
                
                List<Vector2> path = pathArea.FindPath(transform.position, point);
                FaceLeft(point.x < viewRoot.transform.position.x);
                follower.StopFollowing();
                if (path != null && path.Count > 0) {
                    viewAnimator.SetTrigger("Walk");
                    follower.Follow(path, () => {
                        viewAnimator.SetTrigger("Idle");
                        Debug.Log("The player has arrived");
                        if (onArrive != null) onArrive.Invoke();
                    });
                }
            }
        }

        public void Update() {
            if (isFaceLeft != wasFaceLeft) {
                FaceLeft(isFaceLeft);
            }
        }

        public void FaceLeft(bool isLeft) {
            Transform viewTransform = view.transform;
            bool isCurrentLeft = viewTransform.localScale.x < 0;
            if (isCurrentLeft != isLeft) {
                viewTransform.localScale = new Vector3(-viewTransform.localScale.x, viewTransform.localScale.y, viewTransform.localScale.z);
            }
            wasFaceLeft = isLeft;
            isFaceLeft = isLeft;
        }

        public void OnPlayerSelected() {
            PerspectiveController.SetPerspective(isModernPerspective);
            CharacterController.SetActivePlayerCharacter(this);
        }
    }

}
