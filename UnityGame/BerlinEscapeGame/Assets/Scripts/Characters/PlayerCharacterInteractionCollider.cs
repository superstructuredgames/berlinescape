﻿using Assets.Scripts.PlayerInteraction;
using UnityEngine;

namespace Assets.Scripts.Characters {
    public class PlayerCharacterInteractionCollider : MonoBehaviour, PlayerInteractable {

        public PlayerCharacter character;

        public void OnPlayerSelected() {
            character.OnPlayerSelected();
        }
    }
}
