﻿using System.Collections.Generic;
using Assets.Scripts.PlayerInteraction;
using UnityEngine;

namespace Assets.Scripts.Characters {
    internal class CharacterController : MonoBehaviour {

        private readonly List<Character> playerCharacters = new List<Character>();
        private int activeIndex;
        
        public void Start() {
            PlayerInteractionController.AddUnboundedInteraction(0, MoveOnAnywhereClickPlayer);
        }

        private void MoveOnAnywhereClickPlayer(Vector2 point) {
            playerCharacters[activeIndex].PathToPoint(point, () => {
                
            });
        }

        private static CharacterController GetInstance() {
            return FindObjectOfType<CharacterController>();
        }

        public static void AddPlayerCharacter(Character character) {
            GetInstance().playerCharacters.Add(character);
        }

        public static void SetActivePlayerCharacter(Character active) {
            CharacterController controller = GetInstance();
            controller.activeIndex = controller.playerCharacters.IndexOf(active);
        }

        public static Character GetActivePlayerCharacter() {
            CharacterController controller = GetInstance();
            return controller.playerCharacters[controller.activeIndex];
        }
    }
}
