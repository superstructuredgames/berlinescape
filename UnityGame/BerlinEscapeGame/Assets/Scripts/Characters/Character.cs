﻿using System;
using UnityEngine;

namespace Assets.Scripts.Characters {
    interface Character {
        void PathToPoint(Vector2 destinationPosition, Action action);
        void FaceLeft(bool isFaceLeft);
    }
}
