﻿using UnityEngine;

namespace Assets.Scripts.PlayerInteraction {
    interface PlayerInteractable {

        void OnPlayerSelected();
    }
}
