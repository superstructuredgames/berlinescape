﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.PlayerInteraction {
    internal class PlayerInteractionController : MonoBehaviour {

        private static readonly Collider2D[] RESULTS = new Collider2D[100];
        private static readonly UnboundedInteractionSort SORT = new UnboundedInteractionSort();
        
        private class UnboundedInteraction {
            public int priority;
            public Action<Vector2> action;
        }

        private class UnboundedInteractionSort : IComparer<UnboundedInteraction> {
            public int Compare(UnboundedInteraction x, UnboundedInteraction y) {
                return x.priority - y.priority;
            }
        }
        private static PlayerInteractionController instance;
        private readonly List<UnboundedInteraction> unboundedInteractions = new List<UnboundedInteraction>();
        private Action<Vector2> overrideAction;
        

        public void Awake() {
            instance = this;
        }

        public void Update() {
            if (Input.GetMouseButtonUp(0)) {
                Vector2 worldPoint = GetCursorPosition();
                bool wasConsumed = false;
                if (overrideAction != null) {
                    Action<Vector2> tempAction = overrideAction;
                    overrideAction = null;
                    tempAction.Invoke(worldPoint);
                    return;
                }
                int hitCount = Physics2D.OverlapPointNonAlloc(worldPoint, RESULTS);
                for (int i = 0; i < hitCount; i++) {
                    PlayerInteractable[] interactables = RESULTS[i].GetComponents<PlayerInteractable>();
                    foreach (PlayerInteractable playerInteractable in interactables) {
                        wasConsumed = true;
                        playerInteractable.OnPlayerSelected();
                    }
                }
                if (!wasConsumed) {
                    if (unboundedInteractions.Count > 0) {
                        unboundedInteractions[0].action.Invoke(worldPoint);
                    }
                }
            }
        }

        public static void AddOneTimeOverride(Action<Vector2> overrideAction) {
            instance.overrideAction = overrideAction;
        }

        public static void AddUnboundedInteraction(int priority, Action<Vector2> action) {
            
            instance.unboundedInteractions.Add(new UnboundedInteraction {
                priority =  priority,
                action = action
            });
            instance.unboundedInteractions.Sort(SORT);
        }

        public static Vector2 GetCursorPosition() {
            Vector2 position = Input.mousePosition;
            return Camera.main.ScreenToWorldPoint(position);
        }
    }
}
