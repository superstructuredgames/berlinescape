﻿using Assets.Scripts.Inventory;
using Assets.Scripts.PlayerInteraction;
using MarkedUi;
using UnityEngine;

namespace Assets.Scripts.UserInterfaces {
    public class InventoryScreenController : MonoBehaviour {

        private MarkedUiView view;

        private int oldItemCount;

        public void OnViewLoad(MarkedUiView aView){
            view = aView;
        }
        public void OnCollapseClicked() {
            view.Switch("Main", "right-left");
        }

        public void OnItemSelectStart(string strIndex) {
            int index = int.Parse(strIndex);
            InventoryController.GetItem(index).HandleSelected();
        }

        public void OnItemSelectStop(string strIndex) {
            int index = int.Parse(strIndex);
            Vector2 position = PlayerInteractionController.GetCursorPosition();
            InventoryController.GetItem(index).HandleUsed(position);
        }

        public void Update() {
            int itemCount = InventoryController.GetItemCount();
            if (itemCount > oldItemCount) {
                for (int i = oldItemCount; i < itemCount; i++) {
                    view.AddElement("id=inventoryroot", GetInventoryElement(InventoryController.GetItem(i), i));
                }
                view.UpdateLayout("id=inventoryroot");
                oldItemCount = itemCount;
            }
                
        }

        private static string GetInventoryElement(InventoryItem item, int itemIndex) {
            string spriteName = item.uiSpriteName;
            return string.Format("<inventory-item image-sprite='{0}' on-item-select-start='OnItemSelectStart' on-item-select-stop='OnItemSelectStop' width='80px' item-index='{1}' id='item-{1}' />", spriteName, itemIndex);
        }
    }
}
