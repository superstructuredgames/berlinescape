﻿using MarkedUi;
using UnityEngine;

namespace Assets.Scripts.UserInterfaces {
    public class MainScreenController : MonoBehaviour {
        private MarkedUiView view;

        public void OnExpandClicked() {
            view.Switch("Inventory", "left-right");
        }


        public void OnViewLoad(MarkedUiView aView) {
            view = aView;
        }
    }
}
