﻿using UnityEngine;

namespace Assets.Scripts.Perspective {
    public interface Perceptible {
        bool IsModern { get; }
        bool IsViewable { get; }
        void TransitionAway(AnimationCurve curve);
        void TransitionIn(AnimationCurve curve);
    }
}
