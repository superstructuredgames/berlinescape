﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Security.Policy;
using UnityEditorInternal;
using UnityEngine;

namespace Assets.Scripts.Perspective {
    public class PerspectiveSprite : MonoBehaviour, Perceptible{
        public bool isModern;

        private bool isViewable;
        private SpriteRenderer sprite;

        public bool IsModern { get { return isModern; } }
        public bool IsViewable { get { return isViewable; } }


        public void Awake() {
            sprite = GetComponent<SpriteRenderer>();
            Color color = sprite.color;
            color.a = 0;
            sprite.color = color;
        }

        public void Start() {
            PerspectiveController.AddSprite(this);
            if (PerspectiveController.IsModernPerspective == isModern) {
                TransitionIn(new AnimationCurve());
            }
        }


        public void TransitionAway(AnimationCurve transitionCurve) {
            StopAllCoroutines();
            Color hidden = sprite.color;
            hidden.a = 0;
            StartCoroutine(Transition(sprite.color, hidden, transitionCurve));
            isViewable = false;
        }

        public void TransitionIn(AnimationCurve transitionCurve) {
            StopAllCoroutines();
            Color visible = sprite.color;
            visible.a = 1;
            StartCoroutine(Transition(sprite.color, visible, transitionCurve));
            isViewable = true;
        }

        private IEnumerator<YieldInstruction> Transition(Color start, Color stop, AnimationCurve curve) {
            float duration = curve.length > 0 ? curve[curve.length - 1].time : 0;
            float startTime = Time.time;
            while (Time.time - startTime <= duration) {
                sprite.color = Color.Lerp(start, stop, curve.Evaluate(Time.time - startTime));
                yield return new WaitForEndOfFrame();
            }
            sprite.color = stop;
        }
        
    }
}
