﻿using System.Collections.Generic;
using System.Security;
using NUnit.Framework.Constraints;
using UnityEngine;

namespace Assets.Scripts.Perspective {
    public class PerspectiveController : MonoBehaviour {

        public static bool IsModernPerspective { get { return instance.isModern; } }
        public AnimationCurve transitionCurve;
        
        private static PerspectiveController instance;
        private readonly List<Perceptible> sprites = new List<Perceptible>();
        private bool isModern;
        
        

        public void Awake() {
            instance = this;
        }

        public static void SetPerspective(bool isModern) {
            bool wasModern = instance.isModern;
            if (wasModern != isModern) {
                instance.isModern = isModern;
                instance.UpdateForPerspectiveShift();
            }
        }

        public static void AddSprite(Perceptible item) {
            instance.sprites.Add(item);
        }

        public static void RemoveItem(Perceptible item) {
            instance.sprites.Remove(item);
        }

        private void UpdateForPerspectiveShift() {
            foreach (Perceptible sprite in sprites) {
                if (sprite.IsModern != isModern && sprite.IsViewable) {
                    sprite.TransitionAway(transitionCurve);
                } else if (sprite.IsModern == isModern && !sprite.IsViewable) {
                    sprite.TransitionIn(transitionCurve);
                }
            }
        }
    }
}
