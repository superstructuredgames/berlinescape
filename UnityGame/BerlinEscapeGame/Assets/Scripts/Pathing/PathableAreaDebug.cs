﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Pathing {
    public class PathableAreaDebug : MonoBehaviour {

        public Transform startTransform;
        public Transform endTransform;
        public PointPathFollower follower;
        public PathableArea area;

        private List<Vector2> points;

        public void Update() {
            if (Input.GetKeyUp(KeyCode.Alpha1)) {
                Vector2 start = startTransform.position;
                Vector2 end = endTransform.position;
                points = area.FindPath(start, end);
                if (points != null) {
                    follower.Follow(points, () => {
                        
                    });
                    
                }
            }
            if (points != null) {
                for (int i = 0; i < points.Count - 1; i++)                {
                    Debug.DrawLine(points[i], points[i + 1], Color.red);
                }
            }
        }
    }
}
