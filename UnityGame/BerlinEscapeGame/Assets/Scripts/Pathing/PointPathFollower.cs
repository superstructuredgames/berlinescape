﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Pathing {
    public class PointPathFollower : MonoBehaviour {

        public float moveSpeed = 1;
        public float arrivalSqDistance = .01f;
        public Transform followRoot;
        private Coroutine activeFollow;

        public void Follow(List<Vector2> path, Action onArrive) {
            if (path != null) {
                activeFollow = StartCoroutine(FollowPath(path, onArrive));
            } else {
                if (onArrive != null) onArrive.Invoke();
            }
            
        }


        private IEnumerator<YieldInstruction> FollowPath(List<Vector2> path, Action onArrive) {
            int currentIndex = 0;
            while (currentIndex < path.Count) {
                yield return new WaitForEndOfFrame();
                float delta = Time.deltaTime;
                Vector2 next = path[currentIndex];
                Vector2 position = followRoot.position;
                Vector2 direction = next - position;
                Vector2 directionNormal = direction.normalized;
                Vector2 toMove = directionNormal * (moveSpeed*delta);
                if (direction.sqrMagnitude < toMove.sqrMagnitude) {
                    toMove = direction;
                }
                position = position + toMove;
                followRoot.position = position;
                if ((position - next).sqrMagnitude <= arrivalSqDistance) {
                    currentIndex++;
                }
            }
            activeFollow = null;
            if (onArrive != null) onArrive.Invoke();
        }

        public void StopFollowing() {
            if (activeFollow != null) {
                StopCoroutine(activeFollow);
            }
        }
    }
}
