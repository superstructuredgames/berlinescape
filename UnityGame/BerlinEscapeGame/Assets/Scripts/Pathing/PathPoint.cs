﻿using System;

namespace Assets.Scripts.Pathing {
    internal struct PathPoint : IEquatable<PathPoint> {

        public int x, y;

        public bool IsValid() {
            return x >= 0 && y >= 0;
        }

        public bool Equals(int x, int y) {
            return this.x == x && this.y == y;
        }

        public bool Equals(PathPoint other) {
            return x == other.x && y == other.y;
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            return obj is PathPoint && Equals((PathPoint) obj);
        }

        public override int GetHashCode() {
            unchecked {
                return (x*397) ^ y;
            }
        }

        public static bool operator ==(PathPoint left, PathPoint right) {
            return left.Equals(right);
        }

        public static bool operator !=(PathPoint left, PathPoint right) {
            return !left.Equals(right);
        }

        public bool IsEitherGreaterEqual(PathPoint other) {
            return x >= other.x || y >= other.y;
        }

        public override string ToString() {
            return string.Format("{{X: {0}, Y: {1}}}", x, y);
        }
    }
}
