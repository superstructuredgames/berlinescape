﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Pathing {
    public class PathableArea : MonoBehaviour {

        private readonly PathSearch search = new AStarSearch();
        public bool isDrawDebug;
        public float resolution = .1f;
        private PolygonCollider2D pathCollider;
        
        private Dictionary<PathPoint, bool> buckets;
        private PathPoint areaSize;
        
        public void Start() {
            pathCollider = GetComponent<PolygonCollider2D>();
            Bucket();
        }

        public void OnDrawGizmos() {
            PathableAreaGizmos.OnDrawGizmos(pathCollider, buckets, transform, resolution, areaSize);
        }

        public List<Vector2> FindPath(Vector2 start, Vector2 end) {
            List<Vector2> path = null;
            PathPoint startPoint = GetPathPoint(start);
            PathPoint endPoint = GetPathPoint(end);
            if (startPoint.IsValid() && endPoint.IsValid()) {
                List<PathPoint> pathPoints = search.GetPath(startPoint, endPoint, areaSize, buckets);
                if (pathPoints != null) {
                    path = new List<Vector2>(pathPoints.Count);
                    foreach (PathPoint pathPoint in pathPoints) {
                        path.Add(GetVectorPoint(pathPoint));
                    }
                }
                
            }
            return path;
        }

        private PathPoint GetPathPoint(Vector2 positon) {
            Bounds bounds = pathCollider.bounds;
            Vector2 scaledRes = transform.lossyScale * resolution;
            positon = positon - new Vector2(bounds.min.x, bounds.min.y);
            int x = (int)(positon.x / scaledRes.x);
            int y = (int)(positon.y / scaledRes.y);
            PathPoint pathPoint = new PathPoint { x = -1, y = -1 };
            if (x > 0 && x < areaSize.x && y > 0 && y < areaSize.y ) {
                pathPoint = new PathPoint { x = x, y = y };
            }
            return pathPoint;
        }

        private Vector2 GetVectorPoint(PathPoint position) {
            Bounds bounds = pathCollider.bounds;
            Vector2 scaledRes = transform.lossyScale * resolution;
            Vector2 point = new Vector2(position.x * scaledRes.x, position.y * scaledRes.y);
            point = point + (Vector2)bounds.min + (scaledRes *.5f);
            return point;
        }
        
        public void Bucket() {
            Bounds bounds = pathCollider.bounds;
            Vector2 min = bounds.min;
            Vector2 max = bounds.max;
            Vector2 scaledRes = transform.lossyScale*resolution;
            int xCount = (int)((max.x - min.x)/scaledRes.x);
            int yCount = (int)((max.y - min.y) / scaledRes.y);
            buckets = new Dictionary<PathPoint, bool>();
            areaSize = new PathPoint {x = xCount, y = yCount};
            for (int x = 0; x < xCount; x++) {
                float xPoint = min.x + (scaledRes.x/2.0f) + x*scaledRes.x;
                for (int y = 0; y < yCount; y++) { 
                    Vector2 point = new Vector2(xPoint, min.y + (scaledRes.y / 2.0f) + y * scaledRes.y);
                    buckets[new PathPoint {x=x, y=y}] = pathCollider.OverlapPoint(point);
                }
            }
        }

        public bool ContainsPoint(Vector3 point) {
            return pathCollider.OverlapPoint(point);
        }
    }
}
