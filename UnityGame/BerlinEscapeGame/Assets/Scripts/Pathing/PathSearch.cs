﻿using System.Collections.Generic;

namespace Assets.Scripts.Pathing {
    internal interface PathSearch {
        List<PathPoint> GetPath(PathPoint startPoint,
            PathPoint endPoint,
            PathPoint bounds,
            Dictionary<PathPoint, bool> area);
    }
}
