﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Pathing {
    internal class TerribleSearch : PathSearch {

        private const long TIME_OUT = 10000000L;
        private static long startTicks;

        private class SearchNode {
            private readonly PathPoint point;
            private readonly SearchNode parent;
            private readonly int height;
            
            public SearchNode(PathPoint point, SearchNode parent, int height) {
                this.point = point;
                this.parent = parent;
                this.height = height;
            }

            public PathPoint Point {
                get { return point; }
            }

            public int Height { get { return height; }  }

            public SearchNode AddChild(int difX, int difY) {
                return new SearchNode(new PathPoint{x=Point.x + difX, y=Point.y + difY}, this, height+1);
            }

            public List<PathPoint> GetPath() {
                Stack<PathPoint> path = new Stack<PathPoint>();
                SearchNode check = this;
                while (check != null) {
                    path.Push(check.point);
                    check = check.parent;
                }
                return path.ToList();
            }
        }
        
        public List<PathPoint> GetPath(PathPoint startPoint, PathPoint endPoint, PathPoint bounds, Dictionary<PathPoint, bool> area) {
            SearchNode head = new SearchNode(startPoint, null, 0);
            Debug.Log("going from " + startPoint + " to " + endPoint);
            List<SearchNode> resolutions = new List<SearchNode>();
            startTicks = DateTime.Now.Ticks;
            DoSearch(head, endPoint, resolutions, bounds, area, new HashSet<PathPoint>(), int.MaxValue);
            Debug.Log("Found"  + resolutions.Count);
            int shortest = int.MaxValue;
            SearchNode resolution = null;
            foreach (SearchNode searchNode in resolutions) {
                if (searchNode.Height < shortest) {
                    resolution = searchNode;
                    shortest = searchNode.Height;
                }
            }
            return resolution != null ? resolution.GetPath() : null;
        }

        private static int DoSearch(SearchNode current, PathPoint endPoint, List<SearchNode> resolutions, PathPoint bounds, Dictionary<PathPoint, bool> area, HashSet<PathPoint> unavail, int solutionHeight) {
            if (DateTime.Now.Ticks - startTicks > TIME_OUT) {
                throw new Exception("Search timed out at " + current.Height);
            }
            if (current.Height > solutionHeight) {
                return solutionHeight;
            }
            if (unavail.Contains(current.Point)) {
                return solutionHeight;
            }
            if (!current.Point.IsValid()) {
                return solutionHeight;
            }
            if (current.Point.IsEitherGreaterEqual(bounds)){
                return solutionHeight;
            }
            if (!area[current.Point]) {
                return solutionHeight;
            }
            if (current.Point == endPoint) {
                solutionHeight = current.Height;
                resolutions.Add(current);
                return solutionHeight;
            }
            unavail.Add(current.Point);
            solutionHeight = DoSearch(current.AddChild(-1, 0), endPoint, resolutions, bounds, area, unavail, solutionHeight);
            solutionHeight = DoSearch(current.AddChild(1, 0), endPoint, resolutions, bounds, area, unavail, solutionHeight);
            solutionHeight = DoSearch(current.AddChild(0, 1), endPoint, resolutions, bounds, area, unavail, solutionHeight);
            solutionHeight = DoSearch(current.AddChild(0, -1), endPoint, resolutions, bounds, area, unavail, solutionHeight);
            //unavail.Remove(current.Point);
            return solutionHeight;
        }
    }
}
