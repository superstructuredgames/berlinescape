﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Pathing {
    internal class PathableAreaGizmos {
        public static void OnDrawGizmos(PolygonCollider2D pathCollider, Dictionary<PathPoint, bool> buckets, Transform transform, float resolution, PathPoint areaSize) {
            if (Application.isPlaying) {
                Bounds bounds = pathCollider.bounds;
                int xCount = areaSize.x;
                int yCount = areaSize.y;
                Vector2 min = bounds.min;
                Vector2 scaledRes = transform.lossyScale * resolution;
                for (int x = 0; x < xCount; x++) {
                    float xPoint = min.x + (scaledRes.x/2.0f) + x*scaledRes.x;
                    for (int y = 0; y < yCount; y++) { 
                        Vector2 point = new Vector2(xPoint, min.y + (scaledRes.y / 2.0f) + y * scaledRes.y);
                        if (buckets[new PathPoint {x=x, y=y}]) {
                            Gizmos.DrawCube(new Vector3(point.x, point.y, transform.position.z), transform.lossyScale * resolution);
                        }
                    }
                }
            }
        }
    }
}
