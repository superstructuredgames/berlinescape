﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Assets.Scripts.Pathing {
    internal class AStarSearch :PathSearch {
        private class  SearchNodeComparer : IComparer<SearchNode> {
            public int Compare(SearchNode a, SearchNode b) {
                return a.cost - b.cost;
            }
        }

        private class SearchNode {
            public PathPoint point;
            public int cost;
            public int directCost;
            public SearchNode parent;
        }

        private static readonly IComparer<SearchNode> COMPARER = new SearchNodeComparer();

        public List<PathPoint> GetPath(PathPoint startPoint, PathPoint endPoint, PathPoint bounds, Dictionary<PathPoint, bool> area) {

            Dictionary<PathPoint, SearchNode> openByPosition = new Dictionary<PathPoint, SearchNode>();

            List<SearchNode> openSet = new List<SearchNode>();
            Dictionary<PathPoint, SearchNode> closedSet = new Dictionary<PathPoint, SearchNode>();

            SearchNode node = new SearchNode { point = startPoint, cost = 0, parent = null, directCost = 0};
            SearchNode endNode = null;
            openSet.Add(node);
            openByPosition.Add(node.point, node);

            while (true) {
                if (openSet.Count == 0){
                    break;
                }
                int leastCost = int.MaxValue;
                int leastIndex = 0;
                for (int i = 0; i < openSet.Count; i++) {
                    if (openSet[i].cost < leastCost) {
                        leastIndex = i;
                        leastCost = openSet[i].cost;
                    }
                }
                node = openSet[leastIndex];
                openSet.RemoveAt(leastIndex);
                openByPosition.Remove(node.point);
                closedSet.Add(node.point, node);
                if (node.point == endPoint) {
                    endNode = node;
                    break;
                }
                
                PossibleAdd(openSet, openByPosition, node, -1, -1, endPoint, bounds, area, closedSet);
                PossibleAdd(openSet, openByPosition, node, -1, 0, endPoint, bounds, area, closedSet);
                PossibleAdd(openSet, openByPosition, node, -1, 1, endPoint, bounds, area, closedSet);
                PossibleAdd(openSet, openByPosition, node, 0, -1, endPoint, bounds, area, closedSet);
                PossibleAdd(openSet, openByPosition, node, 0, 1, endPoint, bounds, area, closedSet);
                PossibleAdd(openSet, openByPosition, node, 1, -1, endPoint, bounds, area, closedSet);
                PossibleAdd(openSet, openByPosition, node, 1, 0, endPoint, bounds, area, closedSet);
                PossibleAdd(openSet, openByPosition, node, 1, 1, endPoint, bounds, area, closedSet);
                openSet.Sort(COMPARER);
            }

            Stack<PathPoint> pathStack = new Stack<PathPoint>();
            while (endNode != null) {
                pathStack.Push(endNode.point);
                endNode = endNode.parent;
            }
            
            return pathStack.ToList();
        }

        private static void PossibleAdd(ICollection<SearchNode> openSet, IDictionary<PathPoint, SearchNode> openByposition, SearchNode parent, int x, int y, PathPoint endPoint, PathPoint bounds, IDictionary<PathPoint, bool> area, IDictionary<PathPoint, SearchNode> closedSet ) {
            PathPoint position = new PathPoint {x=parent.point.x + x, y=parent.point.y + y};
            if (!position.IsValid()) {
                return;
            }
            if (position.IsEitherGreaterEqual(bounds)) {
                return;
            }
            if (!area[position]) {
                return;
            }
            if (closedSet.ContainsKey(position)) {
                return;
            }
            int directCost = 10;
            if (x != 0 && y != 0){
                directCost = 14;
            }
            int pathCost = directCost;
            SearchNode travelParent = parent;
            while (travelParent != null) {
                pathCost += travelParent.directCost;
                travelParent = travelParent.parent;
            }

            int cost = pathCost + (Math.Abs(position.x - endPoint.x) + Math.Abs(position.y - endPoint.y)) * 10;
            if (openByposition.ContainsKey(position)) {
                SearchNode existing = openByposition[position];
                if (existing.cost > cost) {
                    existing.parent = parent;
                    existing.cost = cost;
                    existing.directCost = directCost;
                }
            } else {
                SearchNode node = new SearchNode { point = position, cost = cost, parent = parent, directCost = directCost};
                openSet.Add(node);
                openByposition.Add(node.point, node);
            }
        }
        
    }
}
