﻿using Assets.Scripts.Inventory;
using Assets.Scripts.PlayerInteraction;
using UnityEngine;
using CharacterController = Assets.Scripts.Characters.CharacterController;


namespace Assets.Scripts.PuzzleElements {
    public class CharacterInventoryPickupInteraction : MonoBehaviour, PlayerInteractable {

        public InventoryItem inventoryItem;
        public Transform arrivalPoint;
        public void OnPlayerSelected() {
            if (inventoryItem != null) {
                CharacterController.GetActivePlayerCharacter().PathToPoint(arrivalPoint.position, OnArrive);
            }
        }

        private void OnArrive() {
            if (inventoryItem != null) {
                InventoryController.AddInventoryItem(inventoryItem);
            }
        }
    }
}
