﻿using System;
using UnityEngine;

namespace Assets.Scripts.PuzzleElements {
    public class HookPullyAnim : MonoBehaviour {

        public event Action OnHookArrive;

        public void FireArrive() {
            if (OnHookArrive != null) {
                OnHookArrive.Invoke();
            }
        }
    }
}
