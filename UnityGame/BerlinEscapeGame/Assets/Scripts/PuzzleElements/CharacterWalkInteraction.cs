﻿using Assets.Scripts.PlayerInteraction;
using UnityEngine;
using CharacterController = Assets.Scripts.Characters.CharacterController;

namespace Assets.Scripts.PuzzleElements {
    public class CharacterWalkInteraction : MonoBehaviour, PlayerInteractable {

        public Transform destination;

        public void OnPlayerSelected() {
            CharacterController.GetActivePlayerCharacter().PathToPoint(destination.position, () => {

            });
        }
    }
}
