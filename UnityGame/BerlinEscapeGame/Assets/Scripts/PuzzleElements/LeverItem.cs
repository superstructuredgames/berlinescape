﻿using System;
using Assets.Scripts.Characters;
using Assets.Scripts.PlayerInteraction;
using UnityEngine;
using CharacterController = Assets.Scripts.Characters.CharacterController;

namespace Assets.Scripts.PuzzleElements {
    public class LeverItem : MonoBehaviour, PlayerInteractable {

        public Collider2D interactionCollider;
        public Transform interactionPoint;
        public Animator anim;
        public LeverState state;
        public bool isLeftFaceOnInteract;
        public enum LeverState {
            Left, Right, Broken
        }

        public event Action<LeverState> OnToggle;
        
        private LeverState oldState;
        private bool isToggleEnabled;

        public void Update() {
            if (state != oldState) {
                anim.SetTrigger(state.ToString());
                oldState = state;
            }
        }

        public void Break() {
            state = LeverState.Broken;
            interactionCollider.gameObject.SetActive(false);
        }

        public void OnPlayerSelected() {
            if (isToggleEnabled) {
                Character character = CharacterController.GetActivePlayerCharacter();
                character.PathToPoint(interactionPoint.position, () => {
                    Debug.Log("toggle");
                    state = state == LeverState.Left ? LeverState.Right : LeverState.Left;
                    character.FaceLeft(isLeftFaceOnInteract);
                    if (OnToggle != null)
                    {
                        OnToggle.Invoke(state);
                    }
                });
            }
            
        }

        public void EnableToggle() {
            isToggleEnabled = true;
        }

        public void DisableToggle() {
            isToggleEnabled = false;
        }
    }
}
