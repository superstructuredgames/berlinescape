﻿using UnityEngine;

namespace Assets.Scripts.PuzzleElements {
    public class HookPullyItem : MonoBehaviour {

        public LeverItem lever;
        public bool isLeft;
        
        public void Start() {
            lever.OnToggle += HandleLeverToggled;
            GetComponentInChildren<HookPullyAnim>().OnHookArrive += HandleHookArrive;
            Animator anim = GetComponentInChildren<Animator>();
            anim.SetTrigger(isLeft ? "Left" : "Right");
        }

        private void HandleHookArrive() {
            lever.EnableToggle();
        }

        private void HandleLeverToggled(LeverItem.LeverState state) {
            Animator anim = GetComponentInChildren<Animator>();
            if (state == LeverItem.LeverState.Right) {
                anim.SetTrigger("Right");
                lever.DisableToggle();
            } else if (state == LeverItem.LeverState.Left) {
                anim.SetTrigger("Left");
                lever.DisableToggle();
            }
        }
    }
}
