﻿using System.Globalization;
using MarkedUi;
using UnityEngine;

namespace Assets.RDG.MarkedUi.Example
{
    [ExecuteInEditMode]
    internal class MarkedUiExampleViewController : MonoBehaviour{

        [SerializeField, HideInInspector] private MarkedUiView view;

        private int btnCount;
        public void OnAddButton() {
            btnCount++;
            view.AddElement("scroll-panel:0/panel:0", "<button height='75px' width='50px' on-click='OnDynamicButtonClicked:" + btnCount + "' text-value='btn" + btnCount + "'/>");
            view.UpdateLayout("scroll-panel:0/panel:0");
        }

        public void OnRemoveButton() {
            btnCount--;
            view.RemoveElement("scroll-panel:0/panel:0/button:0");
            view.UpdateLayout("scroll-panel:0/panel:0");
        }

        public void OnDynamicButtonClicked(string data) {
            Debug.Log("We Clicked buton '" + data + "'!");
        }

        public void OnExampleSelectOneClicked(string value) {
            Debug.Log("select one now has value '" + value + '\'');
        }

        public void OnExampleToggle(bool value) {
            string text = value ? "alternate" : "normal";
            view.ReplaceElement("id=alternate-option", string.Format("<select-one-opt id='alternate-option' option-text='{0}' option-value='OPT2'/>", text));
            view.UpdateParentLayout("id=alternate-option");
        }

        public void OnExampleSliderChanged(float value) {
            view.SetAttribute("id=slide-label", "text-value", value.ToString(CultureInfo.InvariantCulture));
            view.UpdateLayout("id=slide-label");
        }

        public void OnExample1Clicked() {
            view.Switch("Example 1", "swipe-down");
        }

        public void OnExample2Clicked() {
            view.Switch("Example 2", "swipe-up");
        }
        
        public void OnViewLoad(MarkedUiView aView) {
            view = aView;
            Debug.Log("The View Has Been Loaded");
        }
    }

    


}
